// public class TestAccount {
//     public static void main(String[] args) {
//         // Account myAccount = new Account();
//         // myAccount.setBalance((double)(10));
//         // myAccount.setName("Michael");
//         // System.out.println("Name: " + myAccount.getName() + ", Balance: " + myAccount.getBalance());
//         // myAccount.addInterest();
//         // System.out.println("Name: " + myAccount.getName() + ", Balance after interest: " + myAccount.getBalance());

//         Account[] arrayOfAccounts = new Account[5];
//         double[] amounts = {11,12,13,14,15};
//         String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

//         for (int i = 0; i < arrayOfAccounts.length;i++){
//             arrayOfAccounts[i] = new Account(); //its abstract now!

//             // this would not save the object in the array
//             // Account curr_acc = arrayOfAccounts[i];
//             // curr_acc = new Account();

//             arrayOfAccounts[i].setBalance(amounts[i]);
//             arrayOfAccounts[i].setName(names[i]);
//             System.out.println("account " + i + "name: " + arrayOfAccounts[i].getName() + ", balance: " + arrayOfAccounts[i].getBalance());
//             arrayOfAccounts[i].addInterest();
//             System.out.println("added interest: " + arrayOfAccounts[i].getBalance());
//         }

//         System.out.println(arrayOfAccounts[2].getName());
//     }
    
// }