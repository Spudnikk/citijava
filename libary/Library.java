import java.util.*;

public class Library implements Display{

    private static HashMap<Integer, libraryItems> items = new HashMap<Integer, libraryItems>();
    private static HashMap<Integer, Integer> borrowed = new HashMap<Integer, Integer>();
    // private static HashMap<Integer, Integer> reservedList = new HashMap<Integer, Integer>();
    

    public static void addItems(int itemID, libraryItems.itemType type){
        switch(type){
            case Periodical:
                items.put(itemID, new Periodicals(itemID, libraryItems.itemType.Periodical));
                break;
            case Book:
                items.put(itemID, new Books(itemID, libraryItems.itemType.Book));
                break;
            case CD:
                items.put(itemID, new Books(itemID, libraryItems.itemType.CD));
                break;
        }
    }

    public static void remove(int itemID){
        if (items.containsKey(itemID)){
            items.remove(itemID);
            System.out.println("Item "+itemID + " is removed");
        }else{
            System.out.println("Item "+itemID+" is not in the system.");
        }
    }

	public void show(){
        for (Map.Entry<Integer, libraryItems> me : items.entrySet()){
            libraryItems val = me.getValue();
            System.out.println("Item ID: " + val.getItem_ID() + ", Item type: " + val.getItem_type() + ", Item status: " + val.getStatus());
        }

        if (borrowed.size()>0){
            System.out.println("Borrowed list: ");
        }
        for (Map.Entry<Integer, Integer> me : borrowed.entrySet()){
            System.out.println("Item ID: " + me.getKey() + ", User ID: " + me.getValue());
        }
    }

    public static void borrow(int item_ID, int user_ID){

        if (items.containsKey(item_ID)){
            libraryItems val = items.get(item_ID);

            if (val.getItem_type() == libraryItems.itemType.Periodical){
                System.out.println("Item is periodical, it cannot be borrowed.");
            }
            else if (val.getStatus() == "Available"){
                List<Integer> user = Users.getUserList().get(user_ID);
                user.add(val.getItem_ID());
                val.setStatus("Borrowed");
                borrowed.put(item_ID, user_ID);
                System.out.println("Item " + item_ID + " borrowed successfully by user " + user_ID);
            }

            //add reserve function
            // else if (val.getStatus() == "Reserved"){

            // }

            else if (val.getStatus() == "Borrowed"){
                System.out.println("Item already borrowed by user "+borrowed.get(item_ID));
            }

        }else{
            System.out.println("Invalid ID, please enter again");
        }
    }

    public static void ret(int itemID, int userID){ //assume userID will always be correct
        List<Integer> user = Users.getUserList().get(userID);
        if (user.contains(itemID)){
            user.remove(itemID);
            borrowed.remove(itemID);
            libraryItems val = items.get(itemID);
            if (val.getStatus() == "borrowed"){
                val.setStatus("available");
            }
            //add reserve function here
            System.out.println("Item " + itemID + " returned successfully");
        }else{
            System.out.println("User " + userID + " did not borrow item " + itemID);
        }
    }
    
}