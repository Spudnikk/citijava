
public abstract class libraryItems{
	enum itemType{
        Book,
        Periodical,
        CD
    }
    private int item_ID;
    private itemType item_type;
    private String status;

    //call this when initializing a library item
    public libraryItems(int item_ID, itemType item_type){
        this.item_ID = item_ID;
        this.item_type = item_type;
        this.status = "Available";
    }

	public int getItem_ID() {
		return item_ID;
	}

	public void setItem_ID(int item_ID) {
		this.item_ID = item_ID;
	}

	public itemType getItem_type() {
		return item_type;
	}

	public void setItem_type(itemType item_type) {
		this.item_type = item_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
}