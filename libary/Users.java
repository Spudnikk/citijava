import java.util.*;
public class Users implements Display{
    enum userType{
        Admin,
        Member
    }
    private static HashMap<Integer, List<Integer>> userList = new HashMap<Integer, List<Integer>>();
    private static HashMap<Integer, userType> userTypes = new HashMap<Integer, userType>();
    private static int currUserID = 1;
    //you can only set userList or userTypes through register User, so we only have getters.

    public static HashMap<Integer, List<Integer>> getUserList() {
        return userList;
    }

    public static void RegisterUser(userType type){
        userList.put(currUserID, new ArrayList<Integer>());
        userTypes.put(currUserID, type);
        System.out.println("Welcome! Your user ID is " + currUserID + " and your user type is "+type);
        currUserID++;
    }

    public void show(){
        System.out.println("User list: ");
        for (Map.Entry<Integer, List<Integer>> me : userList.entrySet()){
            System.out.println("User ID: " + me.getKey() + ", User borrowed list: "+me.getValue());
        }
    }

    public static HashMap<Integer, userType> getUserTypes() {
        return userTypes;
    }
    
}