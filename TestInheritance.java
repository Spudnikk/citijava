public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = new Account[3];

        accounts[0] = new SavingsAccount("A",2);
        accounts[1] = new CurrentAccount("B",4);
        accounts[2] = new SavingsAccount("C",6);

        for (int i = 0; i < accounts.length;i++){
            accounts[i].addInterest();
            System.out.println(accounts[i].getName() + " "+accounts[i].getBalance());
        }

    }
    
}