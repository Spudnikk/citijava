public abstract class Account implements Detailable{

    //properties
    private Double balance;
    private String name;
    private static double interestRate;

    public String getDetails(){
        return "Balance: " + balance;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Account() {
        this.name = "Michael";
        this.balance = 50.0;
        
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }
    
    //overloading
    public boolean withdraw(double amount){
        if (amount <= this.balance){
            this.balance -= amount;
            System.out.println("transaction completed");
            return true;
        }
        System.out.println("transaction failed");
        return false;
    }

    public boolean withdraw(){
        double amount = 20;
        if (amount <= this.balance){
            this.balance -= amount;
            System.out.println("transaction completed");
            return true;
        }
        System.out.println("transaction failed");
        return false;
    }    
}


